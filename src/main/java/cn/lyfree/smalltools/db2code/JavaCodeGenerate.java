package cn.lyfree.smalltools.db2code;

import cn.lyfree.smalltools.db2code.meta.DBInfo;
import cn.lyfree.smalltools.utils.FileUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.apache.log4j.Logger;
import org.dom4j.Element;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class JavaCodeGenerate {
        private static Logger logger= Logger.getLogger(JavaCodeGenerate.class);
    public JavaCodeGenerate(Element rootElement) {
        super();
        this.rootElement = rootElement;
        mappersElement = rootElement.element("mappers");
        templatesElement = rootElement.element("templates");
        dbinfoElement = rootElement.element("dbinfo");

    }

    private final Element rootElement;
    private final Element templatesElement;
    private final Element mappersElement;
    private final Element dbinfoElement;
    private DBInfo connInfo = null;
    private final HashMap<String, String> tabClsMap = new HashMap<>();
    private final HashSet<String> tabCreate = new HashSet<>();
    private final HashMap<String,String> outDir= new HashMap<>();

    private void init() throws IOException {
        initFreemarker();
        initOutDir();
        initTabClsMap();
    }
    Configuration freemarkerCfg = null;

    private void initFreemarker() throws IOException {

        freemarkerCfg = new Configuration(Configuration.VERSION_2_3_23);

        File tplDir = new File(FileUtils.getClassPath(templatesElement.attributeValue("templateroot", "")));
        logger.info(tplDir.getAbsoluteFile());
        freemarkerCfg.setDirectoryForTemplateLoading(tplDir);
        freemarkerCfg.setDefaultEncoding("UTF-8");
        freemarkerCfg.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
    }

    private void initOutDir() {

        File classdir = new File(templatesElement.attributeValue("classroot", FileUtils.getClassPath()));
        if (!classdir.exists()) {
            classdir.mkdirs();
        }
        File webdir = new File(templatesElement.attributeValue("webroot", FileUtils.getClassPath()));
        if (!webdir.exists()) {
            webdir.mkdirs();
        }
        outDir.put("classRoot", classdir.getAbsolutePath());
        outDir.put("webRoot", webdir.getAbsolutePath());

        List<Element> allTemplate = templatesElement.elements("template");
        File codedir;
        for (Element aTemplate : allTemplate) {
            if (Boolean.valueOf(aTemplate.attributeValue("enabled", "false"))) {
                String category = aTemplate.attributeValue("category", "");
                if (category.equals("class")) {
                    codedir = new File(outDir.get("classRoot") + File.separator + aTemplate.attributeValue("package", "").replaceAll("\\.", "\\"+File.separator));
                    if (!codedir.exists()) {
                        codedir.mkdirs();
                       
                    }
                    if(!outDir.containsKey("class-"+aTemplate.attributeValue("name", ""))){
                      outDir.put("class-"+aTemplate.attributeValue("name", ""), codedir.getAbsolutePath());
                    }
                } else if (category.equals("web")) {
                    codedir = new File(outDir.get("webRoot") + File.separator + aTemplate.attributeValue("urlPrefix", ""));
                    if (!codedir.exists()) {
                        codedir.mkdirs();
                    }
                    if(!outDir.containsKey("web-"+aTemplate.attributeValue("name", ""))){
                      outDir.put("web-"+aTemplate.attributeValue("name", ""), codedir.getAbsolutePath());
                    }
                }
            }
        }
    }

    private void initTabClsMap() {
        tabClsMap.clear();
        List<Element> allMapper = mappersElement.elements("mapper");
        for (Element aMapper : allMapper) {
            tabClsMap.put(aMapper.attributeValue("table"), aMapper.attributeValue("object"));
            if (Boolean.valueOf(aMapper.attributeValue("create", "false"))) {
                tabCreate.add(aMapper.attributeValue("table"));
            }
        }
    }

    private String getTemplateOutFileDir(Element aTemplate){
                   
                String category = aTemplate.attributeValue("category", "");
                if (category.equals("web")) {
                  return  outDir.get("web-"+aTemplate.attributeValue("name", "")) ;
                  
                } else{  
                     return   outDir.get("class-"+aTemplate.attributeValue("name", "")); 
                }
            
    }
    public void Generate() throws IOException {

        init();
        CreateDBInfo();
        if (connInfo == null) {
            return;
        }


        String fileName;
        File outFile;
        String objName;
          List<Element> allTemplate = templatesElement.elements("template");
        
        for (String tabName : tabCreate) {
            try {
                logger.info("current table:" + tabName);
                if (tabClsMap.get(tabName).equals("")) {//中间表不用生成类文件
                    logger.info("link table,skip");
                    continue;
                }

                
                for (Element aTemplate : allTemplate) {
                    logger.info("Generate " + aTemplate.attributeValue("name") + "....");

                    objName = tabClsMap.get(tabName);
                    fileName = getTemplateOutFileDir(aTemplate) + File.separator + objName
                            + aTemplate.attributeValue("fileSuffix","");
                    outFile = new File(fileName);
                 if(Boolean.valueOf(aTemplate.attributeValue("enabled","false"))&&
                         (!outFile.exists()||Boolean.valueOf(aTemplate.attributeValue("override","false")))) {
                      GenerateCodeFile(tabName,fileName, aTemplate);
                    }else{
                      logger.info("   skip..");
                 }
                }
            } catch (Exception e) {
                logger.error(e.toString(),e);
                
            }

        }
        logger.info("Generate complete.");
    }

    private void CreateDBInfo() {
        Connection con = null;
        try {
            con = getConnection(dbinfoElement.element("drivename").getTextTrim(), dbinfoElement.element("url").getTextTrim(), dbinfoElement.element("username").getTextTrim(), dbinfoElement.element("password").getTextTrim());
            connInfo = DBInfo.createDBInfo(con, dbinfoElement.element("scheme").getTextTrim(),
                    DBType.valueOf(dbinfoElement.element("dbtype").getTextTrim().toString()), tabCreate);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            connInfo = null;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            connInfo = null;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    connInfo = null;
                }
            }
        }

    }

    private Connection getConnection(String driverName, String url,
            String username, String password) throws ClassNotFoundException,
            SQLException {

        Class.forName(driverName);

        Connection connection = null;
        connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

    private void GenerateCodeFile(String tabName,String outFile, Element aTemplate) throws  Exception {
        Map<String, Object> root = new HashMap<String, Object>();
        root.put("tableInfo", connInfo.getTable(tabName));
        root.put("tabClsMap", tabClsMap);
        root.put("root", rootElement);

        Template template = freemarkerCfg.getTemplate(aTemplate.attributeValue("name") + ".flt");
        //Writer out = new OutputStreamWriter(new FileOutputStream(OUT_POJO_PATH+clsName + ".java"),"utf-8");
        //	Writer out = new OutputStreamWriter(System.out,"utf-8");
        StringWriter out = new StringWriter();
        template.process(root, out);
   

        OutputStreamWriter write = new OutputStreamWriter(
                new FileOutputStream(outFile), "utf-8");
        write.append(out.toString());
        write.flush();
        out.close();
        write.close();

    }

}
