package cn.lyfree.smalltools.db2code.meta;

public class ForeignKeyInfo {

	public ForeignKeyInfo(String fkColumnName, String pkColumnName,
			String pkTableName) {
		super();
		this.fkColumnName = fkColumnName;
		this.pkColumnName = pkColumnName;
		this.pkTableName = pkTableName;
	}
	public String getFkColumnName() {
		return fkColumnName;
	}
	public void setFkColumnName(String fkColumnName) {
		this.fkColumnName = fkColumnName;
	}
	public String getPkColumnName() {
		return pkColumnName;
	}
	public void setPkColumnName(String pkColumnName) {
		this.pkColumnName = pkColumnName;
	}
	public String getPkTableName() {
		return pkTableName;
	}
	public void setPkTableName(String pkTableName) {
		this.pkTableName = pkTableName;
	}
	private String fkColumnName;
	private String pkColumnName;
	private String pkTableName;
	
}
