package cn.lyfree.smalltools.db2code.meta;

import cn.lyfree.smalltools.db2code.DBType;
import cn.lyfree.smalltools.db2code.DataTypeDialect;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.HashSet;

public class DBInfo {
        private static Logger logger= Logger.getLogger(DBInfo.class);
	public static DBInfo createDBInfo(Connection conn, String scheme,
			DBType dbtype, HashSet<String> tabCreate) {
		DBInfo dbInfo = new DBInfo();
		try {
			DatabaseMetaData dbMetaData = conn.getMetaData();
			String catalog = conn.getCatalog(); // catalog 其实也就是数据库名
                    try (ResultSet tablesResultSet = dbMetaData.getTables(catalog, scheme,
                            null, new String[] { "TABLE" })) {
                        TableInfo tableInfo = null;
                        ColumnInfo columnInfo = null;
                        
//				ResultSetMetaData meta=tablesResultSet.getMetaData();
//				int colCount=tablesResultSet.getMetaData().getColumnCount();
// 				 for(int k=1;k<colCount+1;k++){
//					 logger.info(meta.getColumnName(k));
//				 }
                        // 循环表信息
                        while (tablesResultSet.next()) {
                            if (tabCreate.contains(tablesResultSet
                                    .getString("TABLE_NAME"))) {
                                if (!dbInfo.getTablesMap().containsKey(
                                        tablesResultSet.getString("TABLE_NAME"))) {
                                    tableInfo = new TableInfo(
                                            tablesResultSet.getString("TABLE_NAME"));
                                    logger.info("create tableInfo:"
                                            + tableInfo.getTableName());
                                } else {
                                    tableInfo = dbInfo.getTablesMap().get(
                                            tablesResultSet.getString("TABLE_NAME"));
                                    logger.info("exists tableInfo:"
                                            + tableInfo.getTableName());
                                }
                                tableInfo.setRemarks(
                                        DataTypeDialect.getTableRemark(dbtype, conn, tableInfo.getTableName(), tablesResultSet)
                                );
                                
                                // 循环列信息
                                try (ResultSet columnsSet = dbMetaData.getColumns(catalog, null,
                                        tableInfo.getTableName(), null)
//					meta=tablesResultSet.getMetaData();
//					  colCount=tablesResultSet.getMetaData().getColumnCount();
//
//		 
//						 for(int k=1;k<colCount+1;k++){
//							 logger.info(meta.getColumnName(k));
//						 }
                                        ) {
                                    // 循环列信息
                                    while (columnsSet.next()) {
                                        
                                        columnInfo = new ColumnInfo(
                                                columnsSet.getString("COLUMN_NAME"),
                                                columnsSet.getString("TYPE_NAME"),
                                                columnsSet.getInt("COLUMN_SIZE"),
                                                columnsSet.getInt("DECIMAL_DIGITS"));
                                        columnInfo.setDbtype(dbtype);
                                        columnInfo.setRemarks(
                                                DataTypeDialect.getColumnRemark(dbtype, conn, tableInfo.getTableName(),columnInfo.getColumnName(),tablesResultSet)
                                        );
                                        // if(columnsSet.getInt("DECIMAL_DIGITS")==0){
                                        // columnInfo= new
                                        // ColumnInfo(columnsSet.getString("COLUMN_NAME"),
                                        // columnsSet.getString("TYPE_NAME"),
                                        // "0");
                                        // }else{
                                        // columnInfo= new
                                        // ColumnInfo(columnsSet.getString("COLUMN_NAME"),
                                        // columnsSet.getString("TYPE_NAME"),
                                        // String.valueOf(columnsSet.getInt("DECIMAL_DIGITS"))
                                        // ,);
                                        // }
                                        tableInfo.getColumns().add(columnInfo);
                                        
                                        logger.info("\t addColumn("
                                                + columnInfo.getColumnName() + ","
                                                + columnInfo.getColumnType() + ","
                                                + columnInfo.getColumnLength() + ","
                                                + columnInfo.getColumnScale() + ")");
                                    }
                                }
                                
                                // 循环主键信息
                                try (ResultSet pkeyset = dbMetaData.getPrimaryKeys(catalog,
                                        null, tableInfo.getTableName())) {
                                    // 循环主键信息
                                    while (pkeyset.next()) {
                                        tableInfo.getPrimaryKeys().put(
                                                pkeyset.getString("COLUMN_NAME"),
                                                pkeyset.getString("PK_NAME"));
                                        logger.info("\t add PK("
                                                + pkeyset.getString("COLUMN_NAME") + ","
                                                + pkeyset.getString("PK_NAME") + ")");
                                    }
                                }
                                
                                try ( // 循环外键
                                        ResultSet foreignKeyResultSet = dbMetaData.getImportedKeys(
                                                catalog, null, tableInfo.getTableName())) {
                                    while (foreignKeyResultSet.next()) {
                                        String fkColumnName = foreignKeyResultSet
                                                .getString("FKCOLUMN_NAME");
                                        String pkTablenName = foreignKeyResultSet
                                                .getString("PKTABLE_NAME");
                                        tableInfo.relateParentTable(fkColumnName,
                                                dbInfo.getOrCreateTable(pkTablenName));
                                        logger.info("\t add FK("
                                                + foreignKeyResultSet
                                                        .getString("FKCOLUMN_NAME") + ","
                                                + foreignKeyResultSet.getString("PKTABLE_NAME")
                                                + ")");
                                        
                                    }
                                }
                                tableInfo.CreateSimpleColumns();
                                dbInfo.getTablesMap().put(tableInfo.getTableName(),
                                        tableInfo);
                            } else {
                                logger.info(" table "
                                        + tablesResultSet.getString("TABLE_NAME")
                                        + " create is false ");
                            }
                            System.out
                                    .println("---------------------------------------------------------------------------------------------------");
			}
                    }
			return dbInfo;

		} catch (Exception ex) {
            ex.printStackTrace();
			return null;
		}

	}

	public HashMap<String, TableInfo> getTablesMap() {
		return tablesMap;
	}

	public void setTablesMap(HashMap<String, TableInfo> tablesMap) {
		this.tablesMap = tablesMap;
	}

	public TableInfo getTable(String tableName) {
		return tablesMap.get(tableName);
	}

	public TableInfo getOrCreateTable(String tableName) {
		if (!tablesMap.containsKey(tableName)) {
			tablesMap.put(tableName, new TableInfo(tableName));
		}
		return tablesMap.get(tableName);
	}

	public void addTable(TableInfo table) {
		tablesMap.put(table.getTableName(), table);
	}

	private HashMap<String, TableInfo> tablesMap = new HashMap<String, TableInfo>();
	
	

}
