package cn.lyfree.smalltools.db2code.meta;

import cn.lyfree.smalltools.db2code.DBType;
import cn.lyfree.smalltools.db2code.DataTypeDialect;

public class ColumnInfo {
	public ColumnInfo(){
	 
	}
	public ColumnInfo(String columnName, String columnType, int columnLength, int columnScale) {
		super();
		this.columnName = columnName;
		this.columnType = columnType;
		this.columnLength=columnLength;
		this.columnScale = columnScale;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public int getColumnScale() {
		return columnScale;
	}
	public void setColumnScale(int columnScale) {
		this.columnScale = columnScale;
	}
	private String columnName;
	private String columnType;
	public final int getColumnLength() {
		return columnLength;
	}
	public final void setColumnLength(int columnLength) {
		this.columnLength = columnLength;
	}
	
	private String remarks;

	public final String getRemarks() {
		return remarks;
	}

	public final void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
   public String getJavaType(){
	   return DataTypeDialect.getJavaType(this,dbType);
   }
	
	private int columnLength;
	private int columnScale;
	public final DBType getDbtype() {
		return dbType;
	}
	public final void setDbtype(DBType dbtype) {
		this.dbType = dbtype;
	}
	private DBType dbType=DBType.ORACLE;
	
}
