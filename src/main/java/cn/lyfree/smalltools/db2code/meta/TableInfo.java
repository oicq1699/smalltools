package cn.lyfree.smalltools.db2code.meta;

import java.util.*;
import java.util.Map.Entry;

/**
 * 表信息，储存与表相关的信息
 *
 * @author ly
 */
public class TableInfo {

    public TableInfo(String tableName) {
        super();
        this.tableName = tableName;
    }

    /**
     * 根据外键列列名获取父表
     *
     * @param fkColumnName 外键列的列名
     * @return 返回父表TableInfo,不存在返回Null
     */
    public TableInfo getParentTable(String fkColumnName) {
        return parentTables.get(fkColumnName);
    }

    /**
     * 根据父表集合索引位置获取父表，主要用在多对多关系中
     *
     * @param idx 索引位置
     * @return 返回父表TableInfo,不存在返回Null
     */
    public Entry<String, TableInfo> getParentTable(int idx) {
        if (idx < 0 || idx > parentTables.size()) {
            return null;
        }
        int currentidx = 0;
        for (Iterator<Entry<String, TableInfo>> it = parentTables.entrySet().iterator(); it.hasNext();) {
            if (idx == currentidx) {
                return it.next();
            } else {
                it.next();
                currentidx++;
            }
        }
        return null;

    }

    /**
     * 根据子表表名返回子表信息
     *
     * @param tableName 子表表名
     * @return 返回子表TableInfo
     */
    public TableInfo getChildTable(String tableName) {
        return childTables.get(tableName);
    }

    public void relateParentTable(String fkColumnName, TableInfo parent) {

        if (!parentTables.containsKey(fkColumnName)) {
            parentTables.put(fkColumnName, parent);
        }
        parent.relateChildTable(this);

    }

    private void relateChildTable(TableInfo child) {
        if (!childTables.containsKey(child.getTableName())) {
            childTables.put(child.tableName, child);
        }
    }

     /**
     * 获取表名
     * @param tableName 
     */
    public String getTableName() {
        return tableName;
    }
    
    /**
     * 设置表名
     * @param tableName 
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String remarks;

    /**
     * 获取表备注
     * @return 
     */
    public final String getRemarks() {
        return remarks;
    }

    public final void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * 获取表的所有列信息，包含外键列
     * @return ColumnInfo集合
     */
    public List<ColumnInfo> getColumns() {
        return columns;
    }

    /**
     * 设置列集合
     * @param columns 
     */
    public void setColumns(List<ColumnInfo> columns) {
        this.columns = columns;
    }
    private String tableName;

    /**
     * 获取主键的键/值集合
     * @return 
     */
    public HashMap<String, String> getPrimaryKeys() {
        return primaryKeys;
    }

    /**
     * 这是一个临时的方法，只针对单主键，以后看来要想办法改进
     *
     * @return
     */
    public String getIdName() {
        if (primaryKeys.size() > 0) {
            return primaryKeys.keySet().toArray()[0].toString().toLowerCase();
        }
        return "";
    }

    public void setPrimaryKeys(HashMap<String, String> primaryKeys) {
        this.primaryKeys = primaryKeys;
    }
    private HashMap<String, String> primaryKeys = new HashMap<String, String>();

    private List<ColumnInfo> columns = new ArrayList<ColumnInfo>();

    public final List<ColumnInfo> getSimpleColumns() {
        return simpleColumns;
    }

    public final void setSimpleColumns(List<ColumnInfo> simpleColumns) {
        this.simpleColumns = simpleColumns;
    }

    private List<ColumnInfo> simpleColumns = new ArrayList<ColumnInfo>();

    public boolean isPrimaryKey(String columnName) {
        return primaryKeys.containsKey(columnName);

    }

    public boolean isForeignKey(String columnName) {
        return parentTables.containsKey(columnName);
    }

    public void CreateSimpleColumns() {
        simpleColumns.clear();
        for (ColumnInfo aColumn : columns) {
            if (!parentTables.containsKey(aColumn.getColumnName())) {
                simpleColumns.add(aColumn);
            }
        }

    }

    /**
     * 父表HashMap集合，以外键列为key进行存储
     */
    public HashMap<String, TableInfo> getParentTables() {
        return parentTables;
    }

    /**
     * 父表HashMap集合，以外键列为key进行存储
     */
    public void setParentTables(LinkedHashMap<String, TableInfo> parentTables) {
        this.parentTables = parentTables;
    }
    /**
     * 父表HashMap集合，以外键列为key进行存储
     */
    private LinkedHashMap<String, TableInfo> parentTables = new LinkedHashMap<String, TableInfo>();

    /**
     * 子表HashMap集合，以表名为key进行存储
     */
    public HashMap<String, TableInfo> getChildTables() {
        return childTables;
    }

    /**
     * 子表HashMap集合，以表名为key进行存储
     */
    public void setChildTables(HashMap<String, TableInfo> childTables) {
        this.childTables = childTables;
    }
    /**
     * 子表HashMap集合，以表名为key进行存储
     */
    private HashMap<String, TableInfo> childTables = new HashMap<String, TableInfo>();

}
