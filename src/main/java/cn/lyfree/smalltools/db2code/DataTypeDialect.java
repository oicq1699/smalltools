
package cn.lyfree.smalltools.db2code;

import cn.lyfree.smalltools.db2code.meta.ColumnInfo;
import cn.lyfree.smalltools.utils.Convert;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author onedear
 * @data:2010-11-15 下午07:54:23
 */
public class DataTypeDialect {

	
	public static String getColumnRemark(DBType dbType,Connection conn,String tableName,String columnName,ResultSet ColumnsResultSet){
		switch(dbType){
		case ORACLE:
			return getOracleColumnRemark(conn,tableName,columnName);
			 
		default:
			try {
				return ColumnsResultSet.getString("REMARKS");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				return "";
			}
		}
	}
	
	public static String getTableRemark(DBType dbType,Connection conn,String tableName,ResultSet tablesResultSet ){
		switch(dbType){
		case ORACLE:
			return getOracleTableRemark(conn,tableName);
			 
		default:
			try {
				return tablesResultSet.getString("REMARKS");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				return "";
			}
		}
		
	}
	
	private static String getOracleColumnRemark(Connection conn,String tableName,String columnName){
		List<Properties> pros=getRows(conn
				, "select comments from user_col_comments t where  t.table_name=? and t.COLUMN_NAME=?"
				, new Object[]{tableName,columnName});
		if(pros.size()>0){
			return pros.get(0).getProperty("comments");
		}else{
		return "";	
		}	
	}
	
	private  static String getOracleTableRemark(Connection conn,String tableName){
		
		List<Properties> pros=getRows(conn
				, "select comments from USER_TAB_COMMENTS t where  t.table_name=?"
				, new Object[]{tableName});
		if(pros.size()>0){
			return pros.get(0).getProperty("comments");
		}else{
		return "";	
		}
		
	}
	
	public  static  List<Properties> getRows(Connection conn,String sql, Object[] params) {
		List<Properties> list =new ArrayList<Properties>();
		PreparedStatement st = null;
		ResultSet rs = null;
 
		try {
			
			st = conn.prepareStatement(sql);
			setParams(params, st);

			rs = st.executeQuery();
			if (rs != null) {
				ResultSetMetaData meta = rs.getMetaData();
				int colCount = rs.getMetaData().getColumnCount();
				Properties pro = null;
				while (rs.next()) {
					pro = new Properties();
					for (int k = 1; k < colCount + 1; k++) {
						String colName = meta.getColumnLabel(k);//getColumnName.(k);
						pro.put(colName.toLowerCase(), 
								Convert.parseStringN(rs.getString(colName)));
					}
					list.add(pro);
				}
			}

		} catch (SQLException e) {
			 e.printStackTrace();
		} finally {
                  if(rs!=null)try {
                      rs.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(DataTypeDialect.class.getName()).log(Level.SEVERE, null, ex);
                  }
		  if(st!=null) try {
                      st.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(DataTypeDialect.class.getName()).log(Level.SEVERE, null, ex);
                  }
		}
		return list;
	}
	
	private  static void setParams(Object[] params, PreparedStatement st)
			throws SQLException {
		if (params != null) {
			int len=params.length;
			for (int pi = 0; pi < len; pi++) {
				if(params[pi] instanceof Date){ 
					st.setTimestamp(pi+1,new Timestamp(((Date)params[pi]).getTime()));
				}else{
				st.setObject(pi+1, params[pi]);
				}
			}
		}
	}
	
	
	
	public static  String getJavaType(ColumnInfo columnInfo, DBType dbType )  {
		
		
		switch(dbType){
		case ORACLE:
			return convertOracleType(columnInfo);
			 
		default:
			return "String";
		}
	}
	
	
		private static String convertOracleType(ColumnInfo columnInfo) {
			String colType=columnInfo.getColumnType().toLowerCase() ;
			if( colType.startsWith("number")){
				
 
                            if(columnInfo.getColumnScale()>0){
					return "Double";
				} else {
                                    if(columnInfo.getColumnLength()==1){
                                        return "Boolean";
                                    }else if(columnInfo.getColumnLength()<=9){
					return "Integer";
                                    }else{
                                        return "Long";
                                    }
				}
			}
			if(colType.startsWith("varchar")||colType.startsWith("nvarchar")|| colType.startsWith("varchar2")||colType.equals("nvarchar2")){
	 
					return "String";
				 
			}
			if(colType.startsWith("date")||colType.startsWith("datetime")||colType.startsWith("timestamp")){
				 
				return "Date";
			 
			}
                        if(colType.equals("blob")){
				 
				return "byte[]";
			 
			}
                        
			return "String";
	}
}

