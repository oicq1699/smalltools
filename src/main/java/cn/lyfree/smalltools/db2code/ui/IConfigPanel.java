/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.lyfree.smalltools.db2code.ui;

import org.dom4j.Element;

/**
 *
 * @author Administrator
 */
public interface IConfigPanel {
    void init(Element rootElement);
    void load();
    void save();
}
