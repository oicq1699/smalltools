package cn.lyfree.smalltools.utils;

import java.io.*;
import java.util.ArrayList;


public class FileUtils {

	 
    public static String readTxtFile(String filePath){
    	return readTxtFile(filePath,"utf-8");
    }
	
    /**
     * 读取资源文件，从类根目录下读取
     * @param fileName
     */
    public  static String  readResFile(String fileName){
    	return readTxtFile(getClassPath()+ fileName,"utf-8");
    }
	
	/**
     * 功能：Java读取txt文件的内容
     * 步骤：1：先获得文件句柄
     * 2：获得文件句柄当做是输入一个字节码流，需要对这个输入流进行读取
     * 3：读取到输入流后，需要读取生成字节流
     * 4：一行一行的输出。readline()。
     * 备注：需要考虑的是异常情况
     * @param filePath
     */
    public static String readTxtFile(String filePath,String encoding){
        try {

            StringBuilder sb= new StringBuilder();    
        	File file=new File(filePath);
                if(file.isFile() && file.exists()){ //判断文件是否存在
                    InputStreamReader read = new InputStreamReader(
                    new FileInputStream(file),encoding);//考虑到编码格式
                    BufferedReader bufferedReader = new BufferedReader(read);
                    String lineTxt = null;
                    while((lineTxt = bufferedReader.readLine()) != null){
                    	sb.append(lineTxt+"\r\n");
                    }
                    read.close();
                    bufferedReader.close();
                    return sb.toString();
        }else{
            System.out.println("找不到指定的文件");
            return null;
        }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
            return null;
        }
     
    }
    
        public static ArrayList<PropertyInfo> readPropertiesFile(String filePath,String encoding){
        try {

                 ArrayList<PropertyInfo> result= new ArrayList<PropertyInfo>();
        	File file=new File(filePath);
                if(file.isFile() && file.exists()){ //判断文件是否存在
                    InputStreamReader read = new InputStreamReader(
                    new FileInputStream(file),encoding);//考虑到编码格式
                    BufferedReader bufferedReader = new BufferedReader(read);
                    String lineTxt = null;
                    while((lineTxt = bufferedReader.readLine()) != null){
                    	if(lineTxt.trim().indexOf('#')!=0 &&lineTxt.trim().indexOf("=")>0){
                            String [] nameValuePair=lineTxt.trim().split("=");
                             result.add(new PropertyInfo(nameValuePair[0], nameValuePair[1]));
                        }
                          //  result.add(new PropertyInfo(lineTxt.trim().sp))
                    }
                    read.close();
                    bufferedReader.close();
                    return result;
        }else{
            System.out.println("找不到指定的文件");
            return null;
        }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
            return null;
        }
     
    }
    
    public static void writeTxtFile(String filePath,String txt,String encoding) throws IOException{
    	File file=new File(filePath);
    	OutputStreamWriter write = new OutputStreamWriter(
    			new FileOutputStream(file),encoding);  
    	write.append(txt);
    	write.flush();
    	write.close();
    }
    
    public static String getClassPath(String relativePath){
    
    	return getClassPath()+relativePath;
    }
    
	public static String getClassPath() {
		String path = FileUtils.class.getResource("/").getPath();
		if (path.startsWith("/"))
			path = path.substring(1);
		return path;
	}
      

}
