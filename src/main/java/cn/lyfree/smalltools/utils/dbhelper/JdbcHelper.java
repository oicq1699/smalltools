package cn.lyfree.smalltools.utils.dbhelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;


public class JdbcHelper extends DBHelper {

	private String driverName;
	private String jdbc_url;
	private String userName;
	private String password;
	
    private static Logger logger = LoggerFactory.getLogger(JdbcHelper.class);
	@Override
	public Connection getConnection() {
 		Connection conn = null;
 		//获取属性
 		try {
 			Class.forName(driverName);
 			conn = DriverManager.getConnection(jdbc_url, userName, password);
 		} catch (Exception e) {
 			logger.error("driverName="+driverName+";jdbc_url="+jdbc_url+";userName="+userName,e);
 		} 
 		return conn;
	}

	public JdbcHelper(String driverName, String jdbc_url, String userName, String password) {
		this.driverName = driverName;
		this.jdbc_url = jdbc_url;
		this.userName = userName;
		this.password = password;
	}

	
//	public static void main(String[] args){
		
	//	DBHelper dbhelper= new JdbcHelper("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@192.168.10.21:1521:orcl", "reap", "tz");
	//	java.sql.Date date =new java.sql.Date(new Date().getTime());
	//	List<Properties> rows= dbhelper.getRows("select * from AD_TASK t where t.ASSIGNDATE>?", new Object[]{date});
	//	List<Properties> rows= dbhelper.getRows("select * from ACT_RE_DEPLOYMENT t where t.DEPLOY_TIME_>?", new Object[]{new Date()});
	//	for(int i=0;i<rows.size();i++){
			
	//	}
		
		
//	}
	
	
	
}
