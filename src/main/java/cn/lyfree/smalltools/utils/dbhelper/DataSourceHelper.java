package cn.lyfree.smalltools.utils.dbhelper;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class DataSourceHelper extends DBHelper {

	private DataSource dataSource;
	public DataSourceHelper(DataSource dataSource){
		this.dataSource=dataSource;
	}
	
	@Override
	public Connection getConnection() {
		// TODO Auto-generated method stub
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
