package cn.lyfree.smalltools.utils.dbhelper;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.logicalcobwebs.proxool.configuration.JAXPConfigurator;

public class ProxoolHelper extends DBHelper {
	private static Log logger = LogFactory.getLog(ProxoolHelper.class);
	static {
		init();
	}

	private static void init() {

		// ��ʼ����ݿ��������ò���

		InputStream in = ProxoolHelper.class
				.getResourceAsStream("/proxool.xml");
		Reader reader = null;
		try {
			reader = new InputStreamReader(in, "gbk");
			JAXPConfigurator.configure(reader, false);
		} catch (Exception ex) {
			logger.error("init error", ex);
		}

	}

	private String aliasName;

	public ProxoolHelper(String aliasName) {
		this.aliasName = aliasName;
	}

	public Connection getConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("proxool." + aliasName);
		} catch (SQLException e) {

			logger.error("getConn error", e);
		}
		return conn;

	}

	public List getStringArrayList(String sql) {
		return getStringArrayList(sql, null);
	}

	public List<String[]> getStringArrayList(String sql, Object[] params) {
		List<String[]> list = null;
		Connection con = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.prepareStatement(sql);
			if (params != null) {
				int len = params.length;
				for (int pi = 0; pi < len; pi++) {
					st.setObject(pi + 1, params[pi]);
				}
			}
			rs = st.executeQuery();
			if (rs != null) {
				int colCount = rs.getMetaData().getColumnCount();
				String[] s = null;
				list = new ArrayList<String[]>();
				while (rs.next()) {
					s = new String[colCount];
					for (int k = 0; k < colCount; k++) {
						s[k] = rs.getString(k + 1);

					}
					list.add(s);
				}
			}

		} catch (SQLException e) {
			logger.error("getStringArrayList error", e);
		} finally {
			closeInfo(con, st, rs);
		}
		return list;
	}

	public void closeInfo(Connection con, Statement st, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (st != null) {
				st.close();
				st = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (con != null) {
				con.close();
				con = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
