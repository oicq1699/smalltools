package cn.lyfree.smalltools.utils.dbhelper;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class JndiHelper extends DBHelper {
	
	private String jndiName; //"java:/BIGSCREEN"
    private static Log logger = LogFactory.getLog(JndiHelper.class);
	public JndiHelper(String jndiName){
		this.jndiName=jndiName;
	}
	public  Connection getConnection() {
		Connection con = null;
		try {
			javax.sql.DataSource ds;
			Context ctx = new InitialContext();
			ds = (javax.sql.DataSource) ctx.lookup(jndiName);
			con = ds.getConnection();

		} catch (Exception e) {
			logger.error("jndiName="+jndiName,e);
		}
		return con;
	}



}
