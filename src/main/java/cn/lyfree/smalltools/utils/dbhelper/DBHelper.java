package cn.lyfree.smalltools.utils.dbhelper;

import cn.lyfree.smalltools.utils.Convert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;


public abstract class DBHelper {
	
	
	
	private static Logger logger = LoggerFactory.getLogger(DBHelper.class);
	public  abstract Connection getConnection() ;
	
	public boolean autoCloseConneciton=true;
	
	private Connection currentConn=null;
	public   String getCountSql(String querySql) {
		String countSql = "select count(0) from (";
		countSql = countSql + " " + querySql+" )";
		System.out.println("countSql=========>"+countSql);
		return countSql;
	}

	
	/**
	 * 根据查询语句返回Properties列表
	 * 注意：Properties中列名全改成小写了（不管你sql语句里是大写还是小写）
	 * @param sql
	 * @param params
	 * @return
	 */
	
	public   List<Properties> getRows(String sql, Object[] params) {
		List<Properties> list = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		if(currentConn ==null){
			currentConn = getConnection();
		}
		try {
			
			st = currentConn.prepareStatement(sql);
			setParams(params, st);

			rs = st.executeQuery();
			if (rs != null) {
				ResultSetMetaData meta = rs.getMetaData();
				int colCount = rs.getMetaData().getColumnCount();
				list = new ArrayList<Properties>();
				Properties pro = null;
				while (rs.next()) {
					pro = new Properties();
					for (int k = 1; k < colCount + 1; k++) {
						String colName = meta.getColumnLabel(k);//getColumnName.(k);
						pro.put(colName.toLowerCase(), 
								Convert.parseStringN(rs.getString(colName)));
					}
					list.add(pro);
				}
			}

		} catch (SQLException e) {
			logger.error("ERROR,sql="+sql,e);
		} finally {
			closeStatement( st, rs);
		}
		return list;
	}


	private void setParams(Object[] params, PreparedStatement st)
			throws SQLException {
		if (params != null) {
			int len=params.length;
			for (int pi = 0; pi < len; pi++) {
				if(params[pi] instanceof Date){ 
					st.setTimestamp(pi+1,new Timestamp(((Date)params[pi]).getTime()));
				}else{
				st.setObject(pi+1, params[pi]);
				}
			}
		}
	}

	public   String[] getARowStringArray(String sql, Object[] params) {
		String[] s = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if(currentConn ==null){
				currentConn = getConnection();
			}
			st = currentConn.prepareStatement(sql);
			setParams(params, st);
			rs = st.executeQuery();
			if (rs != null) {
				int colCount = rs.getMetaData().getColumnCount();
				if (rs.next()) {
					s = new String[colCount];
					for (int k = 0; k < colCount; k++) {
						s[k] = rs.getString(k + 1);
					}
				}
			}

		} catch (SQLException e) {
			logger.error("ERROR,sql="+sql,e);
		} finally {
			closeStatement( st, rs);
		}
		return s;
	}

	public   List<String[]> getArrayRows(String sql, Object[] params) throws SQLException {
		List<String[]> list = null;
 
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if(currentConn ==null){
				currentConn = getConnection();
			}
			st = currentConn.prepareStatement(sql);
			setParams(params, st);
			rs = st.executeQuery();
			if (rs != null) {
				int colCount = rs.getMetaData().getColumnCount();
				String[] s = null;
				list = new ArrayList<String[]>();
				while (rs.next()) {
					s = new String[colCount];
					for (int k = 0; k < colCount; k++) {
						s[k] = rs.getString(k + 1);
						
					}
					list.add(s);
				}
			}

		} catch (SQLException e) {
			logger.error("ERROR,sql="+sql,e);
			throw e;
		} finally {
			closeStatement( st, rs);
		}
		return list;
	}

	/**
	  * 根据sql，取得一个字符串的数组列表
	  * @param sql
	  * @return
	  */
	public  String getOnlyStringValue(String sql, Object[] params){
		String[] s = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if(currentConn ==null){
				currentConn = getConnection();
			}
			st = currentConn.prepareStatement(sql);
			setParams(params, st);
			rs = st.executeQuery();
			if (rs != null) {
				int colCount = rs.getMetaData().getColumnCount();
				if (rs.next()) {
					s = new String[colCount];
					for (int k = 0; k < colCount; k++) {
						s[k] = rs.getString(k + 1);
					}
				}
			}

		} catch (SQLException e) {
			logger.error("ERROR,sql="+sql,e);
		} finally {
			closeStatement( st, rs);
		}
	    if(s==null){
	    	return null;
	    }
		return s[0];
	}
	public  boolean runSql(String sql) throws SQLException{
		return runSql(sql,null);
	}
	public   boolean runSql(String sql, Object[] params) throws SQLException {
	       boolean ok=false;

	       PreparedStatement st = null;
	        try {
				if(currentConn ==null){
					currentConn = getConnection();
				}
				st = currentConn.prepareStatement(sql);
				setParams(params, st);
	         st.execute();
	         ok=true;
	       }
	       catch (SQLException ex) {
	    	   logger.error("ERROR,sql="+sql,ex);
	    	   throw ex;
	      }finally{
	          closeStatement(st,null);
	        }
	       return ok;
	}

	public void   closeStatement(Statement st, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (st != null) {
				st.close();
				st = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		 if(isAutoCloseConneciton()){
			 closeConn(currentConn);
		 }
	}

	public  void closeConn(Connection connection){

		try {
			if (connection != null) {
				connection.close();
				connection = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

	public boolean isAutoCloseConneciton() {
		return autoCloseConneciton;
	}


	public void setAutoCloseConneciton(boolean autoCloseConneciton) {
		this.autoCloseConneciton = autoCloseConneciton;
	}
}
