package cn.lyfree.smalltools.utils;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import java.text.NumberFormat;





//import cn.topcheer.realtypricing.dbhelper.FormatDateTimeType;

public  class Convert
{
	public  final static  int DateType_START=1;
	public  final static int DateType_CURRENT=2;
	public  final static int DateType_END=3;

	
	public static long spacingDays(Date startDate,Date endDate)
    {
    	
    	Calendar startCalender=Calendar.getInstance();
    	startCalender.setTime(startDate);
    	
    	Calendar endCalender=Calendar.getInstance();
    	endCalender.setTime(endDate);
    	long result=(endCalender.getTimeInMillis()-startCalender.getTimeInMillis())/
    			(1000*60*60*24);
    	return result;
    }
    public static Date addDate(Date startDate,int addDays)
    {
    	Calendar startCalender=Calendar.getInstance();
    	startCalender.setTime(startDate);
    	startCalender.add(Calendar.DATE, addDays);
    	return startCalender.getTime();
    }
    
    public static Date addHour(Date startDate,int hours)
    {
    	Calendar startCalender=Calendar.getInstance();
    	startCalender.setTime(startDate);
    	startCalender.add(Calendar.HOUR_OF_DAY, hours);
    	return startCalender.getTime();
    }
    
    
    public static Date parseDate(String date)
    {
   		try
			{
				
				String formatStr=analyseDateFormat(date);
				if(formatStr==null)
				{
					DateFormat sd=SimpleDateFormat.getDateTimeInstance();
					return sd.parse(date);
				}
				else
				{
					SimpleDateFormat sdf=new SimpleDateFormat(formatStr);
					return sdf.parse(date);
				}
			}
			catch(ParseException ex)
			{
				ex.printStackTrace();
					return null;
			}
		}
    	

    private  static String analyseDateFormat(String date)
    {
    	String result=null;
    	String input=date.trim();

    	 if(Pattern.matches("\\d{2,4}-\\d{1,2}-\\d{1,2}.*",input))
    	{
    		result= "yyyy-MM-dd";
    	}
    	 else if(Pattern.matches("\\d{1,2}-\\d{1,2}-\\d{2,4}.*",input))
     	{
     	
     		result=  "MM-dd-yyyy";
     	}

    	 else if(Pattern.matches("\\d{2,4}/\\d{1,2}/\\d{1,2}.*",input))
    	{
    	
    		result=  "yyyy/MM/dd";
    	}

    	else if(Pattern.matches("\\d{1,2}/\\d{1,2}/\\d{2,4}.*",input))
    	{
    	
    		result=  "MM/dd/yyyy";
    	}
    	
    	else if(Pattern.matches("\\d{6,8}.*",input))
        {
        		result= "yyyyMMdd";
        }

		if( date.trim().length()>12)
			{
				result=result+" HH:mm:ss";
			}

    	return result;

    }
    
    
    public static Date parseDate(String date,String format)
    {
    	SimpleDateFormat df= new SimpleDateFormat(format);
    	
    	try
		{
			return df.parse(date);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    
    }
    
    public static String formatDate(Date date,String format)
    {
		SimpleDateFormat sdf=new SimpleDateFormat(format);
		return  sdf.format(date);
    }
    public static String formatDate(Date date,int DateType)
    {
    	SimpleDateFormat sdf;
    	switch(DateType)
    	{
    		case DateType_START:
    			 sdf=new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    			break;
    		case DateType_END:
   			 sdf=new SimpleDateFormat("yyyy-MM-dd 23:59:59");
   			 break;
   			 default:
   	   			 sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   			 break;
    			
    	}
		
		return  sdf.format(date);
    }
    
    public static java.sql.Date toSqlDate(Date date)
    {
    	java.sql.Date rv=new java.sql.Date(date.getTime());
    	return rv;
    }
    
    
     public static Date parseDateN(Object date)
    {
    	if(date!=null)
    	{
    		return parseDate(date.toString());
    	}
    	else
    	{
    		return parseDate("1900-01-01");
    	}
    }
    
    public static double parseDoubleN(Object data)
    {
    	if(data!=null)
    	{
    		try
    		{
    			return Double.parseDouble(data.toString());
    		}
    		catch(Exception ex)
    		{
    			return 0;
    		}
    	}
    	else
    	{
    		return 0;
    	}
    }
    
    public static int parseIntN(Object data)
    {
    	if(data!=null)
    	{
    		try
    		{
    			return (int) Double.parseDouble(data.toString());

    		}
    		catch(Exception ex)
    		{
    			return 0;
    		}
    	}
    	else
    	{
    		return 0;
    	}
    	
    }
    
    public static String parseStringN(Object data)
    {
    	if(data!=null)
    	{
    		if(data instanceof String){
    			return (String)data;
    		}
    		return data.toString();
    	}
    	else
    	{
    		return "";
    	}
    }
    
	public static String emptyStr2Null(String oriString) {

		if (oriString != null && oriString.trim().equals("")) {
			return null;
		}
		return oriString;
	}

	public static String nullStr2Empty(String oriString){
	      if (oriString==null||oriString.equals("null"))    return "";
 
	          return oriString;
	}
	
	public  static String[] emptyStrArray2Null(String[] oStringArray) {

		if (oStringArray == null
				|| oStringArray.length == 0
				|| (oStringArray.length == 1 && oStringArray[0].trim().equals(
						""))) {

			return null;
		}
		for (int i = 0; i < oStringArray.length; i++) {
			if (oStringArray[i].trim().equals("")) {

				oStringArray[i] = null;
			}
		}
		return oStringArray;

	}
    
    
    
    public static String getNewUUID()
    {
    	return "{"+UUID.randomUUID().toString().toUpperCase()+"}";
    }
    
    public static String getNewUUID(String leftmarks,String rightmarks)
    {
    	return leftmarks+(UUID.randomUUID().toString().toUpperCase())+rightmarks;
    }
    
    
    public static boolean isNumeric(String str){
    	   Pattern pattern = Pattern.compile("-?[0-9]+.?[0-9]*([Ee]{1}[0-9]+)?");
    	   Matcher isNum = pattern.matcher(str);
    	   if( !isNum.matches() ){
    	       return false;
    	   }
    	   return true;
    	} 
    
    /**
     * 判断字符串是否为空字符或为null
     * @param str
     * @return
     */
    public static boolean isEorN(String str){
    	if(str==null) return true;
    	return str.trim().equals("");
    }

    public static Double round(double data,int scaleBit)
    {
   	 return ((double)Math.round( data*Math.pow(10,scaleBit)))/Math.pow(10,scaleBit);
    }
    
    public static String formatNumber(double data)
    {

    	return formatNumber(data,"###########.####");
    }
    
    public static String formatNumber(double data,String formatPattern)
    {
    	DecimalFormat df= (DecimalFormat)DecimalFormat.getInstance();
    	df.applyPattern(formatPattern);
    	return df.format(data);
    } 

    


	


	

	

	
	public static String join(Object[] s, String delimiter) {
	    StringBuffer buffer = new StringBuffer();
	    for(int i=0;i<s.length;i++)
	    {
	    	buffer.append(s[i]);
	    	if(i<s.length-1)
	    	{
	    		buffer.append(delimiter);
	    	}
	    } 
	    return buffer.toString();
	}
	
	public static void print(String msg){
		System.out.println(Convert.formatDate(new Date(),"yyyy-MM-dd hh:mm:ss:SS>>")+msg);
	}
	
	public static  String cn8859To2312(String utf8_value){
          if(utf8_value==null){
            return null;
          }
          try {
            byte[] b;
            b = utf8_value.getBytes("8859_1"); //中间用ISO-8859-1过渡
            String name = new String(b, "GB2312"); //转换成GB2312字符
            return name;
          }
          catch (UnsupportedEncodingException ex) {
            return null;
          }
       }
	

	public static Matcher matchRegex(String source,String regex,int startidx){
		Pattern p = Pattern.compile(regex,
				Pattern.CASE_INSENSITIVE|Pattern.UNICODE_CASE|Pattern.DOTALL);	
		Matcher result= p.matcher(source);
		 
			if(!result.find(startidx)){	
				return null;
			} else{
				return result;
			}
	}
	
	//	public static void main(String[] args){
	//	//	System.out.println( Convert.formatDate(Convert.addHour(new Date(), 48),Convert.DateType_CURRENT));
	//		System.out.println(String.format("\\'%1$02x", (byte)33  ));
	//		//System.out.println( Convert.formatDate(Convert.addHour(new Date(), 48),Convert.DateType_CURRENT));
	//	}

}

	

